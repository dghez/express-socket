var app = require("express")();
var http = require("http").Server(app);
var io = require("socket.io")(http, { origins: "localhost:*" });

app.get("/", (req, res) => res.send("Hello World!"));

http.listen(9000, () => console.log(`Example app listening on port 9000!`));

io.on("connection", function(socket) {
  console.log("connected");
});

io.emit("startExperience");

io.emit("stopExperience");

io.emit("resumeExperience");

// TOGGLE ON-OFF
setTimeout(() => {
  setInterval(() => {
    io.emit("handDown", "left");
    setTimeout(() => {
      io.emit("handUp", "left");
    }, 3000);
  }, 5000);

  setInterval(() => {
    io.emit("handDown", "right");
    setTimeout(() => {
      io.emit("handUp", "right");
    }, 3000);
  }, 3000);
}, 5000);
