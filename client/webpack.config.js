const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

const devMode = process.env.NODE_ENV !== "production";

module.exports = {
  mode: devMode ? "development" : "production",
  entry: "./src/js",
  output: {
    filename: "index.js"
  },
  resolve: {
    alias: {
      js: path.resolve(__dirname, "src/js")
    }
  },
  plugins: [
    new CleanWebpackPlugin("dist/**/*"),
    new HtmlWebpackPlugin({
      title: "test",
      template: "./src/templates/index.html"
    })
  ],
  module: {
    rules: [
      {
        test: /\.(jpg|png|mp3)$/,
        loader: "file-loader",
        options: { name: "assets/[hash].[ext]" }
      },
      { test: /\.html$/, loader: "html-loader" },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        loader: "file-loader",
        options: { name: "fonts/[hash].[ext]" }
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      }
    ]
  }
};
