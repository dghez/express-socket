import ioClient from "socket.io-client";

var io = ioClient("http://localhost:9000", { transports: ["websocket", "polling", "flashsocket"] });

io.on("toggleButton", value => {
  console.log(`toggle is ${value}`);
});
